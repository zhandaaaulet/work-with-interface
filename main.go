package main

import "fmt"

type Payer interface {
	Pay(int) error
}

type Wallet struct {
	Cash int
}

func (w *Wallet) Pay(amount int) error {
	if w.Cash < amount {
		return fmt.Errorf("Not enough cash in your wallet")
	}
	w.Cash -= amount
	return nil
}

type Card struct {
	Balance    int
	ValidUntil string
	Cardholder string
	CVV        string
	number     string
}

func (c *Card) Pay(amount int) error {
	if c.Balance < amount {
		return fmt.Errorf("Not enough money on balance")
	}
	c.Balance -= amount
	return nil
}

type ApplePay struct {
	Money   int
	AppleID string
}

func (a *ApplePay) Pay(amount int) error {
	if a.Money < amount {
		return fmt.Errorf("Not enough money on account")
	}
	a.Money -= amount
	return nil
}

func Buy(p Payer) {
	switch p.(type) {
	case *Wallet:
		fmt.Println("You pay by Cash")
	case *Card:
		plasticCard := p.(*Card)
		/*if !ok {
			fmt.Println("Cannot convert payer into Card")
		}*/
		fmt.Printf("%v, insert your card please \n", plasticCard)
	default:
		fmt.Println("Something new")

	}
	err := p.Pay(10)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Payment was made by %T \n", p)
}

type Person struct {
	ID   int
	Name string
}

//Receiver by value
func (p Person) UpdateName(name string) {
	p.Name = name
}

//Receiver by pointer
func (p *Person) SetName(name string) {
	p.Name = name
}

func main() {
	/*bob := &Person{Name: "Bob"}
	judy := Person{Name: "Judy"}
	fmt.Printf("Bob has type %T and Judy has type %T", bob, judy)*/

	/*bob := Person{}
	bob.UpdateName("Balenbay")
	fmt.Println(bob)
	bob.SetName("Zhako")
	fmt.Println(bob)*/

	myWallet := &Wallet{Cash: 100}
	Buy(myWallet)

	var myMoney Payer
	myMoney = &Card{Balance: 100, Cardholder: "rvasily"}
	Buy(myMoney)

	myMoney = &ApplePay{Money: 50}
	Buy(myMoney)

}
